package com.demo.easitraveler.ui.broadcast;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.media.MediaCodec;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.demo.easitraveler.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RecordVideoFragment extends Fragment {
    private final int CAMERA_REQUEST = 1;

    private MutableLiveData<Boolean> playing = new MutableLiveData<>();
    private VideoView videoView;
    private Button button;
    private CameraCaptureSession cameraCaptureSession;
    private CameraDevice camera;
    private SurfaceHolder surfaceHolder;
    private HandlerThread cameraHandlerThread;
    private Handler cameraHandler;
    private MediaRecorder mediaRecorder;
    private Uri videoPath;
    private Surface persistentSurface;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_record_video, container, false);

        videoView = root.findViewById(R.id.videoView);
        videoView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    openCamera(holder.getSurfaceFrame().width(), holder.getSurfaceFrame().height());
                    surfaceHolder = holder;
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                closeCamera();
            }
        });

        button = root.findViewById(R.id.play);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TEST", "Toggle Play");
                if (playing.getValue() != null && playing.getValue()) {
                    playing.setValue(false);
                } else {
                    playing.setValue(true);
                }
            }
        });
        playing.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean != null && aBoolean) {
                    Log.d("TEST", "Start Recording");
                    button.setText(R.string.stop);
                    startRecording();
                } else {
                    Log.d("TEST", "Stop Recording");
                    button.setText(R.string.record);
                    stopRecording();
                }
            }
        });

        return root;
    }

    private void requestCameraPermissions() {
        requestPermissions(
                new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                CAMERA_REQUEST
        );
    }

    private void openCamera(int width, int height) throws CameraAccessException {
        if (getActivity() != null) {
            CameraManager cameraManager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
            if (cameraManager != null) {
                String selectedCameraId = null;
                for (String cameraId : cameraManager.getCameraIdList()) {
                    selectedCameraId = cameraId;
                    break;
                }
                if (selectedCameraId != null) {
                    // use a separate thread to handle moving camera data to the screen and the video file
                    cameraHandlerThread = new HandlerThread("camera-" + selectedCameraId);
                    cameraHandlerThread.start();
                    cameraHandler = new Handler(cameraHandlerThread.getLooper());

                    if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestCameraPermissions();
                        return;
                    }
                    cameraManager.openCamera(selectedCameraId, new CameraDevice.StateCallback() {
                        @Override
                        public void onOpened(@NonNull CameraDevice c) {
                            camera = c;
                            try {
                                baseStartVideo();
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onDisconnected(@NonNull CameraDevice camera) {
                            baseStopVideo();
                            closeCamera();
                        }

                        @Override
                        public void onError(@NonNull CameraDevice camera, int error) {
                            baseStopVideo();
                            closeCamera();
                        }
                    }, cameraHandler);
                }
            }
        }
    }

    private void closeCamera() {
        camera.close();
        if (cameraHandlerThread != null) {
            cameraHandlerThread.quit();
            cameraHandlerThread = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (surfaceHolder != null) {
                    try {
                        openCamera(surfaceHolder.getSurfaceFrame().width(), surfaceHolder.getSurfaceFrame().height());
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private boolean shouldSaveToFile() {
        return playing.getValue() != null && playing.getValue();
    }

    private void baseStartVideo() throws CameraAccessException {
        if (surfaceHolder != null && camera != null) {
            List<Surface> outputs = new ArrayList<>();
            outputs.add(surfaceHolder.getSurface());

            persistentSurface = MediaCodec.createPersistentInputSurface();
            outputs.add(persistentSurface);

            camera.createCaptureSession(
                    outputs,
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            cameraCaptureSession = session;
                            try {
                                CaptureRequest.Builder captureRequest = camera.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                                captureRequest.addTarget(surfaceHolder.getSurface());
                                captureRequest.addTarget(persistentSurface);
                                cameraCaptureSession.setRepeatingRequest(
                                        captureRequest.build(),
                                        new CameraCaptureSession.CaptureCallback() {
                                            @Override
                                            public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
                                                super.onCaptureStarted(session, request, timestamp, frameNumber);
                                            }
                                        },
                                        cameraHandler
                                );
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            cameraCaptureSession = null;
                        }
                    },
                    cameraHandler
            );
        }
    }

    private void startRecording() {
        try {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);

            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
            mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);

            mediaRecorder.setInputSurface(persistentSurface);
            if (surfaceHolder != null) {
                mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
            }

            File tmpFile = File.createTempFile("video", ".3gp");
            videoPath = Uri.fromFile(tmpFile);
            mediaRecorder.setOutputFile(videoPath.getPath());
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IOException e) {
            Log.d("TEST", "Media Recorder Error", e);
            mediaRecorder = null;
        }
    }

    private void stopRecording() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;

            if (videoPath != null) {
                if (FirebaseAuth.getInstance().getUid() == null) {
                    FirebaseAuth.getInstance().signInAnonymously().addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            saveVideo();
                        }
                    });
                } else {
                    saveVideo();
                }
            }
        }
    }

    private void saveVideo() {
        StorageReference videoRef = FirebaseStorage.getInstance().getReference().child("videos/" + videoPath.getLastPathSegment());
        videoRef.putFile(videoPath);
        videoPath = null;
        Log.d("TEST", "Saving file test");
    }

    private void baseStopVideo() {
        if (persistentSurface != null) {
            persistentSurface.release();
            persistentSurface = null;
        }
        if (cameraCaptureSession != null) {
            try {
                cameraCaptureSession.stopRepeating();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }
    }
}
