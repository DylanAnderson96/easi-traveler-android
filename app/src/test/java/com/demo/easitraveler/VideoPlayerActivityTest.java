package com.demo.easitraveler;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

import androidx.lifecycle.Lifecycle;
import androidx.test.core.app.ActivityScenario;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@RunWith(AndroidJUnit4.class)
public class VideoPlayerActivityTest {
    @Test
    public void load_video() {
        Intent intent = new Intent(getInstrumentation().getTargetContext(), VideoPlayerActivity.class);
        intent.putExtra("uri", "content://com.demo.easitraveler/easiPitch.mp4");
        ActivityScenario<VideoPlayerActivity> scenario = ActivityScenario.launch(intent);
        scenario.moveToState(Lifecycle.State.DESTROYED);
    }

    @Test
    public void press_back_button() {
        Intent intent = new Intent(getInstrumentation().getTargetContext(), VideoPlayerActivity.class);
        intent.putExtra("uri", "content://com.demo.easitraveler/easiPitch.mp4");
        ActivityScenario<VideoPlayerActivity> scenario = ActivityScenario.launch(intent);
        scenario.onActivity(new ActivityScenario.ActivityAction<VideoPlayerActivity>() {
            @Override
            public void perform(VideoPlayerActivity activity) {
                MenuItem item = new MenuItem() {
                    @Override
                    public int getItemId() {
                        return android.R.id.home;
                    }

                    @Override
                    public int getGroupId() {
                        return 0;
                    }

                    @Override
                    public int getOrder() {
                        return 0;
                    }

                    @Override
                    public MenuItem setTitle(CharSequence title) {
                        return null;
                    }

                    @Override
                    public MenuItem setTitle(int title) {
                        return null;
                    }

                    @Override
                    public CharSequence getTitle() {
                        return null;
                    }

                    @Override
                    public MenuItem setTitleCondensed(CharSequence title) {
                        return null;
                    }

                    @Override
                    public CharSequence getTitleCondensed() {
                        return null;
                    }

                    @Override
                    public MenuItem setIcon(Drawable icon) {
                        return null;
                    }

                    @Override
                    public MenuItem setIcon(int iconRes) {
                        return null;
                    }

                    @Override
                    public Drawable getIcon() {
                        return null;
                    }

                    @Override
                    public MenuItem setIntent(Intent intent) {
                        return null;
                    }

                    @Override
                    public Intent getIntent() {
                        return null;
                    }

                    @Override
                    public MenuItem setShortcut(char numericChar, char alphaChar) {
                        return null;
                    }

                    @Override
                    public MenuItem setNumericShortcut(char numericChar) {
                        return null;
                    }

                    @Override
                    public char getNumericShortcut() {
                        return 0;
                    }

                    @Override
                    public MenuItem setAlphabeticShortcut(char alphaChar) {
                        return null;
                    }

                    @Override
                    public char getAlphabeticShortcut() {
                        return 0;
                    }

                    @Override
                    public MenuItem setCheckable(boolean checkable) {
                        return null;
                    }

                    @Override
                    public boolean isCheckable() {
                        return false;
                    }

                    @Override
                    public MenuItem setChecked(boolean checked) {
                        return null;
                    }

                    @Override
                    public boolean isChecked() {
                        return false;
                    }

                    @Override
                    public MenuItem setVisible(boolean visible) {
                        return null;
                    }

                    @Override
                    public boolean isVisible() {
                        return false;
                    }

                    @Override
                    public MenuItem setEnabled(boolean enabled) {
                        return null;
                    }

                    @Override
                    public boolean isEnabled() {
                        return false;
                    }

                    @Override
                    public boolean hasSubMenu() {
                        return false;
                    }

                    @Override
                    public SubMenu getSubMenu() {
                        return null;
                    }

                    @Override
                    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener menuItemClickListener) {
                        return null;
                    }

                    @Override
                    public ContextMenu.ContextMenuInfo getMenuInfo() {
                        return null;
                    }

                    @Override
                    public void setShowAsAction(int actionEnum) {

                    }

                    @Override
                    public MenuItem setShowAsActionFlags(int actionEnum) {
                        return null;
                    }

                    @Override
                    public MenuItem setActionView(View view) {
                        return null;
                    }

                    @Override
                    public MenuItem setActionView(int resId) {
                        return null;
                    }

                    @Override
                    public View getActionView() {
                        return null;
                    }

                    @Override
                    public MenuItem setActionProvider(ActionProvider actionProvider) {
                        return null;
                    }

                    @Override
                    public ActionProvider getActionProvider() {
                        return null;
                    }

                    @Override
                    public boolean expandActionView() {
                        return false;
                    }

                    @Override
                    public boolean collapseActionView() {
                        return false;
                    }

                    @Override
                    public boolean isActionViewExpanded() {
                        return false;
                    }

                    @Override
                    public MenuItem setOnActionExpandListener(OnActionExpandListener listener) {
                        return null;
                    }
                };
                activity.onOptionsItemSelected(item);
            }
        });
        scenario.moveToState(Lifecycle.State.DESTROYED);
    }
}
