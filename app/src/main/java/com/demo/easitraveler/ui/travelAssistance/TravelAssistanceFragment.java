package com.demo.easitraveler.ui.travelAssistance;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.demo.easitraveler.R;

public class TravelAssistanceFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_travel_assistance, container, false);

        final TextView officeHoursPhoneTextView = root.findViewById(R.id.travel_assistance_office_hour_phone);
        final FrameLayout officeHoursEmailFrameLayout = root.findViewById(R.id.travel_assistance_email);
        final TextView afterHoursPhoneTextView = root.findViewById(R.id.travel_assistance_after_hours_phone);
        final TextView rentalCarIssuesPhoneTextView = root.findViewById(R.id.rental_car_issues_phone);


        officeHoursPhoneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(getString(R.string.travel_assistance_office_hour_phone_uri)));
                startActivity(intent);
            }
        });

        officeHoursEmailFrameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse(getString(R.string.travel_assistance_email_uri)));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Need Travel Assistance");
                intent.putExtra(Intent.EXTRA_TEXT, "Please describe the problem");
                startActivity(Intent.createChooser(intent, "Choose an Email client :"));
            }
        });

        afterHoursPhoneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(getString(R.string.travel_assistance_after_hours_phone_uri)));
                startActivity(intent);
            }
        });

        rentalCarIssuesPhoneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(getString(R.string.rental_car_issues_phone_url)));
                startActivity(intent);
            }
        });

        return root;
    }
}