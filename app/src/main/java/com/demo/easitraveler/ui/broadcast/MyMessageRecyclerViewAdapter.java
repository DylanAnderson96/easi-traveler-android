package com.demo.easitraveler.ui.broadcast;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.easitraveler.R;
import com.demo.easitraveler.dummy.BroadcastMessageContent.BroadcastMessage;
import com.demo.easitraveler.ui.broadcast.MessageFragment.OnListFragmentInteractionListener;
import com.google.android.gms.common.internal.Objects;

/**
 * {@link RecyclerView.Adapter} that can display a {@link BroadcastMessage} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class MyMessageRecyclerViewAdapter extends ListAdapter<BroadcastMessage, MyMessageRecyclerViewAdapter.ViewHolder> {
    private static DiffUtil.ItemCallback<BroadcastMessage> DIFF_CALLBACK = new DiffUtil.ItemCallback<BroadcastMessage>() {
        @Override
        public boolean areItemsTheSame(@NonNull BroadcastMessage oldItem, @NonNull BroadcastMessage newItem) {
            return Objects.equal(oldItem.id, newItem.id);
        }

        @Override
        public boolean areContentsTheSame(@NonNull BroadcastMessage oldItem, @NonNull BroadcastMessage newItem) {
            return Objects.equal(oldItem.text, newItem.text) &&
                    Objects.equal(oldItem.title, newItem.title);
        }
    };

    private OnListFragmentInteractionListener mListener = null;

    MyMessageRecyclerViewAdapter() {
        super(DIFF_CALLBACK);
    }

    void setListener(OnListFragmentInteractionListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = getItem(position);
        holder.mContentView.setText(holder.mItem.text);
        holder.mTitleView.setText(holder.mItem.title);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mContentView;
        final TextView mTitleView;
        BroadcastMessage mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = view.findViewById(R.id.content);
            mTitleView = view.findViewById(R.id.title);
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
