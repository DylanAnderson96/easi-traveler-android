package com.demo.easitraveler.ui.elevatorPitch;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.demo.easitraveler.R;
import com.demo.easitraveler.VideoPlayerActivity;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ElevatorPitchVideoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ElevatorPitchVideoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ElevatorPitchVideoFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    public ElevatorPitchVideoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ElevatorPitchVideoFragment.
     */
    public static ElevatorPitchVideoFragment newInstance() {
        ElevatorPitchVideoFragment fragment = new ElevatorPitchVideoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_elevator_pitch_video, container, false);

        final ImageView videoImageView = root.findViewById(R.id.video);
        videoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSampleVideo();
            }
        });

        return root;
    }

    private void playSampleVideo() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.putExtra("uri", "content://com.demo.easitraveler/easiPitch.mp4");
        intent.setDataAndType(Uri.parse("https://firebasestorage.googleapis.com/v0/b/easi-traveler-95505.appspot.com/o/easiPitch.mp4?alt=media&token=9847c146-237c-4856-9afe-f6e3b4ece13e"), "video/mp4");
        startActivity(intent);
    }

    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onElevatorPitchVideoFragmentInteraction();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onElevatorPitchVideoFragmentInteraction();
    }
}
