package com.demo.easitraveler;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.test.core.app.ActivityScenario;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import java.util.concurrent.Executor;

import static junit.framework.TestCase.assertEquals;

@Ignore("Does not work, fix until it works")
@RunWith(AndroidJUnit4.class)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*", "androidx.*"})
@PrepareForTest({FirebaseApp.class, FirebaseFirestore.class, FirebaseInstanceId.class})
public class BroadcastFragmentTest {
    private FirebaseApp firebaseApp;
    private FirebaseFirestore firestore;
    private FirebaseInstanceId firebaseInstanceId;
    private Task<InstanceIdResult> instanceIdResultTask;

    @Rule
    public PowerMockRule powerMockRule = new PowerMockRule();

//    @Rule
//    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void initialize_firebase_app() {
        firebaseApp = Mockito.mock(FirebaseApp.class);

        PowerMockito.mockStatic(FirebaseApp.class);
        Mockito.when(FirebaseApp.getInstance()).thenReturn(firebaseApp);

        assertEquals(firebaseApp, FirebaseApp.getInstance());
    }

    @Before
    public void initialize_firebase_firestore() {
        firestore = Mockito.mock(FirebaseFirestore.class);

        PowerMockito.mockStatic(FirebaseFirestore.class);
        Mockito.when(FirebaseFirestore.getInstance()).thenReturn(firestore);

        assertEquals(firestore, FirebaseFirestore.getInstance());
    }

    @Before
    public void initialize_firebase_instance_id() {
        firebaseInstanceId = Mockito.mock(FirebaseInstanceId.class);
        instanceIdResultTask = new Task<InstanceIdResult>() {
            @Override
            public boolean isComplete() {
                return false;
            }

            @Override
            public boolean isSuccessful() {
                return false;
            }

            @Override
            public boolean isCanceled() {
                return false;
            }

            @Nullable
            @Override
            public InstanceIdResult getResult() {
                return null;
            }

            @Nullable
            @Override
            public <X extends Throwable> InstanceIdResult getResult(@NonNull Class<X> aClass) throws X {
                return null;
            }

            @Nullable
            @Override
            public Exception getException() {
                return null;
            }

            @NonNull
            @Override
            public Task<InstanceIdResult> addOnSuccessListener(@NonNull OnSuccessListener<? super InstanceIdResult> onSuccessListener) {
                return null;
            }

            @NonNull
            @Override
            public Task<InstanceIdResult> addOnSuccessListener(@NonNull Executor executor, @NonNull OnSuccessListener<? super InstanceIdResult> onSuccessListener) {
                return null;
            }

            @NonNull
            @Override
            public Task<InstanceIdResult> addOnSuccessListener(@NonNull Activity activity, @NonNull OnSuccessListener<? super InstanceIdResult> onSuccessListener) {
                return null;
            }

            @NonNull
            @Override
            public Task<InstanceIdResult> addOnFailureListener(@NonNull OnFailureListener onFailureListener) {
                return null;
            }

            @NonNull
            @Override
            public Task<InstanceIdResult> addOnFailureListener(@NonNull Executor executor, @NonNull OnFailureListener onFailureListener) {
                return null;
            }

            @NonNull
            @Override
            public Task<InstanceIdResult> addOnFailureListener(@NonNull Activity activity, @NonNull OnFailureListener onFailureListener) {
                return null;
            }
        };
        Mockito.when(firebaseInstanceId.getInstanceId()).thenReturn(instanceIdResultTask);

        assertEquals(instanceIdResultTask, firebaseInstanceId.getInstanceId());

        PowerMockito.mockStatic(FirebaseInstanceId.class);
        Mockito.when(FirebaseInstanceId.getInstance()).thenReturn(firebaseInstanceId);

        assertEquals(firebaseInstanceId, FirebaseInstanceId.getInstance());
    }

    @Test
    public void load_broadcast_fragment() {
        ActivityScenario<MainActivity> scenario = ActivityScenario.launch(MainActivity.class);
        scenario.moveToState(Lifecycle.State.CREATED);
        scenario.onActivity(new ActivityScenario.ActivityAction<MainActivity>() {
            @Override
            public void perform(MainActivity activity) {
                activity.navHostFragment.getNavController().navigate(R.id.nav_broadcast);
            }
        });
        scenario.moveToState(Lifecycle.State.DESTROYED);
    }
}
