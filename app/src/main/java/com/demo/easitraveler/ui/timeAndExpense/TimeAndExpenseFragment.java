package com.demo.easitraveler.ui.timeAndExpense;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demo.easitraveler.R;

public class TimeAndExpenseFragment extends Fragment {
    private String lastUrl = "https://timeandexpense.aerotek.com/webtime/";
    private WebView webView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_time_and_expense, container, false);

        webView = root.findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        if (webView.getSettings() != null) {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
        }
        webView.loadUrl(lastUrl);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        lastUrl = webView.getUrl();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState !=  null) {
            webView.restoreState(savedInstanceState);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }
}