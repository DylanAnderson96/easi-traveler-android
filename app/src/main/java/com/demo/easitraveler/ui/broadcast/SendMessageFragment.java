package com.demo.easitraveler.ui.broadcast;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.demo.easitraveler.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSendMessageFragmentListener} interface
 * to handle interaction events.
 * Use the {@link SendMessageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SendMessageFragment extends Fragment {
    private OnSendMessageFragmentListener mListener;
    private EditText messageEditText;
    private Button sendMessageButton;

    public SendMessageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SendMessageFragment.
     */
    static SendMessageFragment newInstance() {
        SendMessageFragment fragment = new SendMessageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_send_message, container, false);

        messageEditText = root.findViewById(R.id.message);
        sendMessageButton = root.findViewById(R.id.send);
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        return root;
    }

    private void sendMessage() {
        Map<String, Object> data = new HashMap<>();
        data.put("text", messageEditText.getText().toString());
        data.put("created", FieldValue.serverTimestamp());

        FirebaseFirestore.getInstance().collection("messages").add(data).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                onSendMessageComplete();
            }
        }).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                sendMessageButton.setEnabled(true);
            }
        });

        sendMessageButton.setEnabled(false);
    }

    private void onSendMessageComplete() {
        if (mListener != null) {
            mListener.onSendMessageComplete();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnSendMessageFragmentListener) {
            mListener = (OnSendMessageFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSendMessageFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSendMessageFragmentListener {
        void onSendMessageComplete();
    }
}
