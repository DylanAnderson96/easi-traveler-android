package com.demo.easitraveler.ui.elevatorPitch;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.demo.easitraveler.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TeamRulesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TeamRulesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeamRulesFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    public TeamRulesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TeamRulesFragment.
     */
    static TeamRulesFragment newInstance() {
        TeamRulesFragment fragment = new TeamRulesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_team_rules, container, false);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed();
            }
        });

        {
            String textToHighlight = "OBLIGATED";
            TextView textView = root.findViewById(R.id.every_individual_is_obligated);
            String originalString = getString(R.string.every_individual_is_obligated);
            String[] parts = originalString.split(textToHighlight);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            if (parts.length == 2) {
                spannableStringBuilder.append(parts[0]);
                spannableStringBuilder.append(textToHighlight);
                spannableStringBuilder.setSpan(
                        new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary, null)),
                        parts[0].length(),
                        parts[0].length() + textToHighlight.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                );
                spannableStringBuilder.setSpan(
                        new StyleSpan(Typeface.BOLD),
                        parts[0].length(),
                        parts[0].length() + textToHighlight.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                );
                spannableStringBuilder.append(parts[1]);

                textView.setText(spannableStringBuilder);
            }
        }

        {
            String textToHighlight = "CARING";
            TextView textView = root.findViewById(R.id.these_thoughts_and_feelings_will_be_shared);
            String originalString = getString(R.string.these_thoughts_and_feelings_will_be_shared);
            String[] parts = originalString.split(textToHighlight);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            if (parts.length == 2) {
                spannableStringBuilder.append(parts[0]);
                spannableStringBuilder.append(textToHighlight);
                spannableStringBuilder.setSpan(
                        new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary, null)),
                        parts[0].length(),
                        parts[0].length() + textToHighlight.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                );
                spannableStringBuilder.setSpan(
                        new StyleSpan(Typeface.BOLD),
                        parts[0].length(),
                        parts[0].length() + textToHighlight.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                );
                spannableStringBuilder.append(parts[1]);

                textView.setText(spannableStringBuilder);
            }
        }

        {
            String textToHighlight = "RESPECT";
            TextView textView = root.findViewById(R.id.this_information_will_be_received);
            String originalString = getString(R.string.this_information_will_be_received);
            String[] parts = originalString.split(textToHighlight);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            if (parts.length == 2) {
                spannableStringBuilder.append(parts[0]);
                spannableStringBuilder.append(textToHighlight);
                spannableStringBuilder.setSpan(
                        new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary, null)),
                        parts[0].length(),
                        parts[0].length() + textToHighlight.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                );
                spannableStringBuilder.setSpan(
                        new StyleSpan(Typeface.BOLD),
                        parts[0].length(),
                        parts[0].length() + textToHighlight.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                );
                spannableStringBuilder.append(parts[1]);

                textView.setText(spannableStringBuilder);
            }
        }

        return root;
    }

    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onTeamRulesFragmentInteraction();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onTeamRulesFragmentInteraction();
    }
}
