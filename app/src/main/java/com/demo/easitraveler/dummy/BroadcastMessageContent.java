package com.demo.easitraveler.dummy;

import androidx.annotation.NonNull;

/**
 * Helper class for providing sample text for user interfaces created by
 * Android template wizards.
 * <p>
 */
public class BroadcastMessageContent {
    /**
     * A dummy item representing a piece of text.
     */
    public static class BroadcastMessage {
        public final String id;
        public final String text;
        public final String title;

        public BroadcastMessage(String id, String text, String title) {
            this.id = id;
            this.text = text;
            this.title = title;
        }

        @NonNull
        @Override
        public String toString() {
            return text;
        }
    }
}
