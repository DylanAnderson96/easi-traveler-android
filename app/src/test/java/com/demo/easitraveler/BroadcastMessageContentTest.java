package com.demo.easitraveler;

import com.demo.easitraveler.dummy.BroadcastMessageContent;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BroadcastMessageContentTest {
    @Test
    public void shouldStoreAndRetrieveData() {
        String id = "id";
        String text = "text";
        String title = "title";

        // should store all data
        BroadcastMessageContent.BroadcastMessage message = new BroadcastMessageContent.BroadcastMessage(
                id,
                text,
                title
        );

        // should retrieve all data
        assertEquals(message.id, id);
        assertEquals(message.title, title);
        assertEquals(message.text, text);

        // should convert to string
        assertEquals(message.toString(), text);
    }
}
