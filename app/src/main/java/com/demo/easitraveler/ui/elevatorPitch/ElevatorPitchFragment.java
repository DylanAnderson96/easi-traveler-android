package com.demo.easitraveler.ui.elevatorPitch;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.demo.easitraveler.MainActivity;
import com.demo.easitraveler.R;
import com.google.android.gms.common.internal.Objects;
import com.google.android.material.tabs.TabLayout;

public class ElevatorPitchFragment extends Fragment {
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_elevator_pitch, container, false);

        TabLayout tabLayout = root.findViewById(R.id.tabs);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                handleSelectedTab(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        TabLayout.Tab selectedTab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
        if (selectedTab != null) {
            handleSelectedTab(selectedTab);
        }

        return root;
    }

    private void handleSelectedTab(TabLayout.Tab selectedTab) {
        if (Objects.equal(selectedTab.getText(), getString(R.string.elevator_pitch_segment))) {
            setSupportActionBarTitle(R.string.menu_elevator_pitch);
            loadElevatorPitchVideoFragment();
        } else if (Objects.equal(selectedTab.getText(), getString(R.string.principles_segment))) {
            setSupportActionBarTitle(R.string.guiding_principles);
            loadGuidingPrinciplesFragment();
        } else if (Objects.equal(selectedTab.getText(), getString(R.string.team_rules))) {
            setSupportActionBarTitle(R.string.team_rules);
            loadTeamRulesFragment();
        }
    }

    private void setSupportActionBarTitle(@StringRes int resId) {
        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            ActionBar actionBar = mainActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(resId);
            }
        }
    }

    /**
     * Load the guiding principles tab.
     */
    private void loadElevatorPitchVideoFragment() {
        FragmentManager fragmentManager = getChildFragmentManager();

        // find existing fragment, to reuse fragment
        ElevatorPitchVideoFragment elevatorPitchVideoFragment = null;
        Fragment existingFragment = fragmentManager.findFragmentByTag("elevatorPitchVideoFragment");
        if (existingFragment instanceof ElevatorPitchVideoFragment) {
            elevatorPitchVideoFragment = (ElevatorPitchVideoFragment) existingFragment;
        }

        // create new fragment if it does not exist
        if (elevatorPitchVideoFragment == null) {
            elevatorPitchVideoFragment = ElevatorPitchVideoFragment.newInstance();
        }

        // swap fragment in tab
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.slot, elevatorPitchVideoFragment, "elevatorPitchVideoFragment");
        fragmentTransaction.commit();
    }

    /**
     * Load the guiding principles tab.
     */
    private void loadGuidingPrinciplesFragment() {
        FragmentManager fragmentManager = getChildFragmentManager();

        // find existing fragment, to reuse fragment
        GuidingPrinciplesFragment guidingPrinciplesFragment = null;
        Fragment existingFragment = fragmentManager.findFragmentByTag("guidingPrinciples");
        if (existingFragment instanceof GuidingPrinciplesFragment) {
            guidingPrinciplesFragment = (GuidingPrinciplesFragment) existingFragment;
        }

        // create new fragment if it does not exist
        if (guidingPrinciplesFragment == null) {
            guidingPrinciplesFragment = GuidingPrinciplesFragment.newInstance();
        }

        // swap fragment in tab
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.slot, guidingPrinciplesFragment, "guidingPrinciples");
        fragmentTransaction.commit();
    }

    /**
     * Load the team rule tab.
     */
    private void loadTeamRulesFragment() {
        FragmentManager fragmentManager = getChildFragmentManager();

        // find existing fragment, to reuse fragment
        TeamRulesFragment teamRulesFragment = null;
        Fragment existingFragment = fragmentManager.findFragmentByTag("teamRules");
        if (existingFragment instanceof TeamRulesFragment) {
            teamRulesFragment = (TeamRulesFragment) existingFragment;
        }

        // create new fragment if it does not exist
        if (teamRulesFragment == null) {
            teamRulesFragment = TeamRulesFragment.newInstance();
        }

        // swap fragment in tab
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.slot, teamRulesFragment, "teamRules");
        fragmentTransaction.commit();
    }
}