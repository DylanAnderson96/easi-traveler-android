package com.demo.easitraveler.ui.broadcast;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.demo.easitraveler.R;

public class BroadcastFragment extends Fragment {
//    private RadioGroup tabRadioGroup;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_broadcast, container, false);

//        tabRadioGroup = root.findViewById(R.id.tabs);
//        if (tabRadioGroup != null) {
//            tabRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(RadioGroup group, int checkedId) {
//                    handleCheckedTabId(checkedId);
//                }
//            });
//            handleCheckedTabId(tabRadioGroup.getCheckedRadioButtonId());
//        }

        loadMessageFragment();

        return root;
    }

//    public void setMessagesTab() {
//        tabRadioGroup.check(R.id.messages);
//    }
//
//    private void handleCheckedTabId(int checkedId) {
//        if (checkedId == R.id.messages) {
//            loadMessageFragment();
//        } else if (checkedId == R.id.record) {
//            loadRecordVideoFragment();
//        }
//    }

    /**
     * Load the messages tab.
     */
    private void loadMessageFragment() {
        FragmentManager fragmentManager = getChildFragmentManager();

        // find existing fragment, to reuse fragment
        MessageFragment messageFragment = null;
        Fragment existingFragment = fragmentManager.findFragmentByTag("messages");
        if (existingFragment instanceof MessageFragment) {
            messageFragment = (MessageFragment) existingFragment;
        }

        // create new fragment if it does not exist
        if (messageFragment == null) {
            messageFragment = MessageFragment.newInstance();
        }

        // swap fragment in tab
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.slot, messageFragment, "messages");
        fragmentTransaction.commit();
    }

//    /**
//     * Load the record video tab.
//     */
//    private void loadRecordVideoFragment() {
//        FragmentManager fragmentManager = getChildFragmentManager();
//
//        // find existing fragment, to reuse fragment
//        RecordVideoFragment recordVideoFragment = null;
//        Fragment existingFragment = fragmentManager.findFragmentByTag("record");
//        if (existingFragment instanceof RecordVideoFragment) {
//            recordVideoFragment = (RecordVideoFragment) existingFragment;
//        }
//
//        // create new fragment if it does not exist
//        if (recordVideoFragment == null) {
//            recordVideoFragment = new RecordVideoFragment();
//        }
//
//        // swap fragment in tab
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.slot, recordVideoFragment, "record");
//        fragmentTransaction.commit();
//    }
}