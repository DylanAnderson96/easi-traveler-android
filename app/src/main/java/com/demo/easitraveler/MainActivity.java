package com.demo.easitraveler;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.demo.easitraveler.dummy.BroadcastMessageContent;
import com.demo.easitraveler.ui.broadcast.MessageFragment;
import com.demo.easitraveler.ui.elevatorPitch.ElevatorPitchVideoFragment;
import com.demo.easitraveler.ui.elevatorPitch.GuidingPrinciplesFragment;
import com.demo.easitraveler.ui.elevatorPitch.TeamRulesFragment;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.testfairy.TestFairy;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements MessageFragment.OnListFragmentInteractionListener,
        ElevatorPitchVideoFragment.OnFragmentInteractionListener,
        GuidingPrinciplesFragment.OnFragmentInteractionListener,
        TeamRulesFragment.OnFragmentInteractionListener
//        SendMessageFragment.OnSendMessageFragmentListener
{

    private AppBarConfiguration mAppBarConfiguration;
    NavHostFragment navHostFragment;
    public String versionCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        TestFairy.begin(this, "SDK-b84KqN8h");
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_concur, R.id.nav_connected, R.id.nav_time_and_expense,
                R.id.nav_outlook, R.id.nav_elevator_pitch, R.id.nav_home,
                R.id.nav_travel_assistance, R.id.nav_privacy_policy)
                .setDrawerLayout(drawer)
                .build();
        drawer.openDrawer(GravityCompat.START);
        Fragment rawNavHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        if (rawNavHostFragment instanceof NavHostFragment) {
            navHostFragment = (NavHostFragment) rawNavHostFragment;
            NavController navController = navHostFragment.getNavController();
            KeepStateNavigator keepStateNavigator = new KeepStateNavigator(this, navHostFragment.getChildFragmentManager());
            navController.getNavigatorProvider().addNavigator(keepStateNavigator);
            navController.setGraph(R.navigation.mobile_navigation);

            NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
            NavigationUI.setupWithNavController(navigationView, navController);

            {
                // the activity was started by a message notification, open broadcast fragment in navController to view messages automatically
                final Intent intent = getIntent();
                if (intent != null && Objects.equal(intent.getStringExtra("notificationAction"), "open_messages")) {
                    navController.navigate(R.id.nav_broadcast);
                }
            }
        }
        try {
            versionCode = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TextView versionName = findViewById(R.id.version_control);
        int currentBuild = BuildConfig.VERSION_CODE;
        versionName.setText("Version " + BuildConfig.VERSION_NAME + " " + getResources().getString(R.string.version_control_string, currentBuild));
        uploadNotificationToken();
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.getPrimaryNavigationFragment();
        if (fragment != null) {
            fragment.onSaveInstanceState(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.getPrimaryNavigationFragment();
        if (fragment != null) {
            fragment.onActivityCreated(savedInstanceState);
        }
    }

    @Override
    public void onListFragmentInteraction(BroadcastMessageContent.BroadcastMessage item) {

    }

//    /**
//     * Action to perform when sending a message is complete.
//     */
//    @Override
//    public void onSendMessageComplete() {
//        if (navHostFragment != null) {
//            FragmentManager fragmentManager = navHostFragment.getChildFragmentManager();
//            Fragment existingFragment = fragmentManager.getPrimaryNavigationFragment();
//            if (existingFragment instanceof BroadcastFragment) {
//                BroadcastFragment broadcastFragment = (BroadcastFragment) existingFragment;
//                broadcastFragment.setMessagesTab();
//            }
//        }
//    }

    private void uploadNotificationToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                uploadNotificationToken2(token);
            }
        });
    }

    private void uploadNotificationToken2(String token) {
        Map<String, Object> data = new HashMap<>();
        data.put("token", token);
        data.put("created", FieldValue.serverTimestamp());

        FirebaseFirestore.getInstance().collection("notificationTokens").document(token).set(data);
    }

    @Override
    public void onElevatorPitchVideoFragmentInteraction() {

    }

    @Override
    public void onGuidingPrinciplesFragmentInteraction() {

    }

    @Override
    public void onTeamRulesFragmentInteraction() {

    }
}
