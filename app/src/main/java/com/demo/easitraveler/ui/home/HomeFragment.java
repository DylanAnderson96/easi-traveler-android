package com.demo.easitraveler.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demo.easitraveler.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static android.content.ContentValues.TAG;

public class HomeFragment extends Fragment implements LocationListener {

    private WebView mWebView = null;
    String mQuery = null;
    RadioButton mButtonAeroTek = null;
    RadioButton mButtonEasi = null;
    private LocationManager locationManager = null;
    private final int MY_PERMISSIONS_REQUEST_READ_LOCATIONS = 100;
    private long ONE_SECOND = 1000L;
    private Double lastLat = null;
    private Double lastLng = null;
    private Double lastZoom = null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        // setup WebView
        mWebView = root.findViewById(R.id.webView);
        mWebView.setWebViewClient(new WebViewClient());
        if (mWebView.getSettings() != null) {
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setDomStorageEnabled(true);
        }
        mWebView.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
        mWebView.addJavascriptInterface(new MapsActivityInterface(this), "Android");
        // open a web page which handle the map
        mWebView.loadUrl("file:///android_asset/index.html");

        // the initial query, AeroTek locations
        mQuery = "EASi";

        // setup AeroTek button
        mButtonAeroTek = root.findViewById(R.id.button_aerotek);
        mButtonAeroTek.setOnClickListener(new MapsActivityOnClick("AeroTek", this));
        mButtonAeroTek.setActivated(true);

        // setup EASi button
        mButtonEasi = root.findViewById(R.id.button_easi);
        mButtonEasi.setOnClickListener(new MapsActivityOnClick("EASi", this));

        // setup GPS Location
        if (getActivity() != null) {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_READ_LOCATIONS
            );
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (lastLat != null && lastLng != null && lastZoom != null) {
            mWebView.evaluateJavascript(
                    "onLocationRestore({latlng: [" +
                            lastLat + "," +
                            lastLng +
                            "], zoom: " + lastZoom + "});",
                    null);
        }
    }

    /**
     * Begin GPS updates. The function will setup automatic gps location updates.
     */
    private void startRequestingLocationUpdates() {
        if (getActivity() != null && getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    ONE_SECOND,
                    1f,
                    this
            );
            Log.d(TAG, "GPS Location Allowed");
        } else {
            Log.d(TAG, "GPS Location Denied");
        }
    }

    /**
     * Request a location immediately and update the WebView.
     */
    void getLastLocationFromDevice() {
        if (getActivity() != null && getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    ONE_SECOND,
                    1f,
                    this
            );
            Log.d(TAG, "GPS Location Allowed");

            Criteria criteria = new Criteria();
            criteria.setPowerRequirement(Criteria.POWER_MEDIUM);

            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (bestProvider != null) {
                Location location = locationManager.getLastKnownLocation(bestProvider);
                if (location != null) {
                    onLocationChanged(location);
                }
            }
        } else {
            Log.d(TAG, "GPS Location Denied");
        }
    }

    /**
     * Stop automatic gps location updates.
     */
    private void stopRequestingLocationUpdates() {
        locationManager.removeUpdates(this);
    }

    /**
     * The activity or app started, begin gps location updates. Also called when the user returns
     * to the app.
     */
    @Override
    public void onStart() {
        super.onStart();
        startRequestingLocationUpdates();
    }

    /**
     * The user changed app, end gps location updates. The user changed to another app, maybe they
     * selected a marker and opened the address in Google Maps.
     */
    @Override
    public void onPause() {
        super.onPause();
        stopRequestingLocationUpdates();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState !=  null) {
            mWebView.restoreState(savedInstanceState);
            lastLat = savedInstanceState.getDouble("lastLat");
            lastLng = savedInstanceState.getDouble("lastLng");
            lastZoom = savedInstanceState.getDouble("lastZoom");
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
        outState.putString("query", mQuery);
        if (lastLat != null) {
            outState.putDouble("lastLat", lastLat);
        }
        if (lastLng != null) {
            outState.putDouble("lastLng", lastLng);
        }
        if (lastZoom != null) {
            outState.putDouble("lastZoom", lastZoom);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mQuery = savedInstanceState.getString("query", "EASi");
            lastLat = savedInstanceState.getDouble("lastLat");
            lastLng = savedInstanceState.getDouble("lastLng");
            lastZoom = savedInstanceState.getDouble("lastZoom");
        }
    }

    /**
     * Handle the result of a permission request. When permission was granted or denied, it will call
     * this function.
     * @param requestCode The id of the permission request. The function handles multiple permission
     *                    requests. Each request type has a unique id.
     * @param permissions A string array.
     * @param grantResults Some data.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_LOCATIONS) {
            startRequestingLocationUpdates();
        }
    }

    /**
     * Open an address in Google Maps. The function is used by marker address links.
     * @param address The address to open in Google Maps.
     */
    void openAddress(@Nullable String address) {
        if (address != null && address.length() > 0) {
            Uri gmmIntentUri = Uri.parse("geo:0,0?q="+address);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    }

    /**
     * Allow javascript to save the map state such as location and zoom so when the phone is tilted
     * side ways, the android app can restore the original map state.
     */
    void saveMapState(Double lat, Double lng, Double zoom) {
        if (lat != null) {
            lastLat = lat;
        }
        if (lng != null) {
            lastLng = lng;
        }
        if (zoom != null) {
            lastZoom = zoom;
        }
    }

    /**
     * Handler for GPS Location changes. It should send the location to JavaScript in the WebView.
     * @param location The GPS Location.
     */
    @Override
    public void onLocationChanged(Location location) {
        mWebView.evaluateJavascript(
                "onLocationFound({latlng: [" +
                        location.getLatitude() + "," +
                        location.getLongitude() +
                        "], accuracy: " + location.getAccuracy() + "});",
                null);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

/**
 * Handle JavaScript to Java calls. The class is exported as "Android" in the WebView and each
 * function is attached to the "Android" object. The class contains functions that only Java can
 * perform such as reading files and opening addresses in the Google Maps app.
 */
class MapsActivityInterface {
    private HomeFragment mHomeFragment;

    MapsActivityInterface(HomeFragment mHomeFragment) {
        this.mHomeFragment = mHomeFragment;
    }

    /**
     * Get markers for the selected Android Button. Java handles AeroTek or EASi location selection
     * and the function will return the selected locations' markers.
     * @return JSON string of marker locations. Contains latlng and addresses.
     */
    @JavascriptInterface
    public String getMarkers() {
        String fileName = "aerotek-locations.json";
        if (mHomeFragment.mQuery.equals("EASi")) {
            fileName = "easi-locations.json";
        }
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = null;
        try {
            if (mHomeFragment.getActivity() != null) {
                reader = new BufferedReader(
                        new InputStreamReader(mHomeFragment.getActivity().getAssets().open(fileName), StandardCharsets.UTF_8)
                );
                String mLine;
                while (true) {
                    mLine = reader.readLine();
                    if (mLine == null) {
                        break;
                    }
                    stringBuilder.append(mLine);
                }
            }
        } catch (IOException e) {
            Log.d(TAG, "Failed to open file with locations", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.d(TAG, "Failed to close file with locations", e);
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Open the address in the Google Maps app.
     * @param address The address to open.
     */
    @JavascriptInterface
    public void openAddress(String address) {
        mHomeFragment.openAddress(address);
    }

    /**
     * Manually load GPS Location. Can be called by JavaScript to load location instead of waiting
     * for a location update.
     */
    @JavascriptInterface
    public void  loadLocation() {
        mHomeFragment.getLastLocationFromDevice();
    }

    /**
     * Save map state so when the phone is tilted side ways, the map can be restored to the original
     * location and zoom before the phone tilt.
     */
    @JavascriptInterface
    public void saveMapState(Double lat, Double lng, Double zoom) {
        mHomeFragment.saveMapState(lat, lng, zoom);
    }
}

/**
 * The button onclick listener which updates the Activity location query and which button is selected.
 */
class MapsActivityOnClick implements View.OnClickListener {
    private String mQuery;
    private HomeFragment mHomeFragment;

    MapsActivityOnClick(String mQuery, HomeFragment mHomeFragment) {
        this.mQuery = mQuery;
        this.mHomeFragment = mHomeFragment;
    }

    /**
     * Handle the button click by updating the query and which button is displayed as selected.
     * @param v The button.
     */
    @Override
    public void onClick(View v) {
        mHomeFragment.mQuery = mQuery;

        if (mQuery.equals("AeroTek")) {
            mHomeFragment.mButtonAeroTek.setActivated(true);
            mHomeFragment.mButtonEasi.setActivated(false);
        } else {
            mHomeFragment.mButtonAeroTek.setActivated(false);
            mHomeFragment.mButtonEasi.setActivated(true);
        }
    }
}
